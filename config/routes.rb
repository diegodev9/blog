Rails.application.routes.draw do
  get 'articles/search'
  devise_for :users, controllers: {
    registrations: "users/registrations",
    sessions: "users/sessions",
    passwords: "users/passwords"
  }
  resources :categories
  resources :articles do
    resources :comments
  end
  # get 'articles/new'
  # get 'articles/show'
  # get 'articles/index'
  # get 'welcome/index'ç
  post 'contacts/new'
  get 'welcome/contacto'
  resources :contacts, only: [:create, :new]
  root 'articles#index'
  
end
