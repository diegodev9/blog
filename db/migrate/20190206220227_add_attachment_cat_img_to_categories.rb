class AddAttachmentCatImgToCategories < ActiveRecord::Migration[5.2]
  def self.up
    change_table :categories do |t|
      t.attachment :cat_img
    end
  end

  def self.down
    remove_attachment :categories, :cat_img
  end
end
