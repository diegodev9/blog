class Contact < ApplicationRecord
  validates :email, :title, :description, presence: true
  validates :title, length: { in: 5..20, too_short: "Mínimo son %{count} caractéres." }
  validates :description, length: { minimum: 50, too_short: "Mínimo son %{count} caractéres." }
end
