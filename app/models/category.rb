class Category < ApplicationRecord
  has_many :has_categories
  has_many :articles, through: :has_categories
  belongs_to :user
  validates :name, uniqueness: true
  validates :name, :color, presence: true
  validates :name, length: { in: 7..20 }
  validates :color, length: { in: 4..7 }

  has_attached_file :cat_img,
    styles:{
      medium: { geometry: '256x170>', format: :png, convert_options: '-background "#c0c0" -gravity center -extent 256x170' }
    },
    default_url: "/images/:style/missing.png"
  validates_attachment_content_type :cat_img, content_type: /\Aimage\/.*\z/

end
