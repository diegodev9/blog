class InfoMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.info_mailer.form_contact.subject
  #
  def form_contact(contacto)
    @correo = Contact.find(contacto.id)
    # raise @correo.to_json
    @greeting = "Hola! #{contacto.email} quiere contactarse contigo."
    mail(to: "diegodev9@gmail.com", subject: @greeting)
  end
end
